import { Component, Input } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend';
  menus;
  constructor(private app: AppService) {
    this.app.menus.subscribe(x=>{ this.menus = x});
  }
}
