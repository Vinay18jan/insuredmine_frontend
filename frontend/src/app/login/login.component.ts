import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginArr = [
    { userid: "abc@media.com", password: "abc123", "username": "tom", ispublic: false },
    { userid: "def@media.com", password: "def123", "username": "dick", ispublic: false }
  ];

  
  email; password; validationMessage = '';menus;
  constructor(private appservice: AppService, private route: Router) { 
  }

  ngOnInit() {
  }

  Validate() {
    const res = this.loginArr.filter((item) => {
      return item.userid === this.email && item.password === this.password;
    });
    if (res.length > 0) {
      this.appservice.setMenu(1);
      this.validationMessage = '';
      this.appservice.setTitle('Hi ' + res[0].username + '' + ', Signout?');
      this.route.navigate(['../home']);
      return true;
    }
    else {
      this.validationMessage = "Login Unsuccessful. Please try again";
      return false;
    }
  }

}
