import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  title = new BehaviorSubject('');
  redirecTo = new BehaviorSubject('');
  menuList= [[
    {
      ispublic: true, m: [
        { name: 'Home', redirectTo: 'home',functionname:''},
        { name: 'About Us', redirectTo: 'aboutus',functionname:''},
        { name: 'Login', redirectTo: 'login' ,functionname:''}
      ]
    }
  ],
  [{  
    ispublic: false, m: [
      { name: 'Home', redirectTo: 'home',functionname:''},
      { name: 'About Us', redirectTo: 'aboutus',functionname:''},
      { name: 'Gallery', redirectTo: 'gallery',functionname:'' }
    ]
  }]
]
  menus = new BehaviorSubject(this.menuList[0]);

  setTitle(title: string) {
    this.title.next(title);
  }

  setRedirectTo(redirecTo: string) {
    this.redirecTo.next(redirecTo);
  }

  setMenu(index){
    this.menus.next(this.menuList[index]);
  }

  constructor() { }
}
