import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public name;
  public redirecTo;

  @Input('menus') menus;
  constructor(private appservice: AppService,private route:Router) { }

  ngOnInit() {
    this.appservice.title.subscribe(x=>{
      this.name = x;
    });
    this.appservice.menus.subscribe(x=>{this.menus=x})
  }

  signout(){
    this.appservice.setMenu(0);
    this.appservice.setTitle('');
    //this.appservice.setRedirectTo('');
    this.route.navigate(['../home']);
  }
}
